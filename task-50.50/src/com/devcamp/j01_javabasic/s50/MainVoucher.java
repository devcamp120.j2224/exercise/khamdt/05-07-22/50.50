package com.devcamp.j01_javabasic.s50;

public class MainVoucher {
    public static void main(String[] args) {
        Vourcher vourcher1 = new Vourcher();
        Vourcher vourcher2 = new Vourcher();
        if (vourcher1 == vourcher2) {
            System.out.println("1. We are the same");
        } else {
            System.out.println("2. We are NOT the same");
        }
        vourcher2 = vourcher1;
        if (vourcher1 == vourcher2) {
            System.out.println("3. We are the same");
        } else {
            System.out.println("4. We are NOT the same");
        }
        vourcher1.setVoucherCode("AMAZING");
        if (vourcher1 == vourcher2) {
            System.out.println("5. We are the same");
        } else {
            System.out.println("6. We are NOT the same");
        }
    }
}
